---
layout: page
permalink: /projects/
hero_desktop: hero-projects-desktop.jpg
hero_mobile: hero-projects-mobile.jpg
title_en:
title_es: Conoce nuestras iniciativas
subtitle_en:
subtitle_es: Estas son las iniciativas para promover los derechos económicos y sociales, en alianza con organizaciones de la sociedad civil a nivel local, nacional y global.
---

{%- include projects.html -%}
